#define N 33
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
int maxV(int *mas)
{
	int i, max = 0;
	for (i = 0;i != N;i++)
		if (mas[i]>max)
			max = mas[i];
	return max;
}
int minV(int *mas)
{
	int i, min = 10000;
	for (i = 0;i != N - 1;i++)
		if (mas[i]<min)
			min = mas[i];
	return min;
}
float averVal(int *mas)
{
	int i;
	float sum = 0;
	for (i = 0;i != N - 1;i++)
		sum += mas[i];
	return sum / N;
}
int main()
{
	int i, sum = 0, max, min;
	float aver;
	int mas[N];
	srand(time(0));
	for (i = 0;i<N - 1;i++)
	{
		mas[i] = rand() % 10000;
		printf("%d ", mas[i]);
	}
	max = maxV(mas);
	min = minV(mas);
	aver = averVal(mas);
	printf("\nMaximal value - %d \nMinimal value -  %d \nAverage value - %.2f\n", max, min, aver);
	return 0;
}