#include <stdio.h>
#include <time.h>
#define N 73


int generate999(int digitNum)
{
    int i,ans=0;
    for (i=1;i<=digitNum;i++)
    {
       ans=ans*10+9;
    }
    return ans;
}
int sumDigits(int num)
{
    int sum=0;
    while(num>0)
    {
        sum+=(num%10);
        num/=10;
    }
    return sum;
}
void countSum(int *arr, int digitNum)
{
    int i;
    for (i=generate999(digitNum);i>0;i--)
    {
        arr[sumDigits(i)]++;
    }
}

void printMas(int *arr, int size)
{
    int i;
    for(i=0;i<size;i++)
    {
        if(arr[i]!=0)
        {
            printf("%d\n",arr[i]);
        }
    }
}

int main()
{
    int i;
    int sums[N]={0};
    clock_t start,finish;
    for(i=1;i<=8;i++)
    {
        start = clock();
        countSum(sums,i);
        finish=clock();
        printf("%f \n", (double)(finish-start));
    }
    return 0;
}

