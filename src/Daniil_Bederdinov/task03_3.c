#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int main()
{
    char str[256];
    int lettercount[256] = { 0 };
    char letter;
    printf("Enter line \n");
    scanf("%s",str);
    for (int i = 0; str[i]!='\0';i++)
        lettercount[str[i]]++;
    for (int i = 0; i < 256;i++)
    {
        if (lettercount[i] != 0)
        {
            letter = i;
            printf("%c - %i, ", letter, lettercount[i]);
        }
    }
    puts("");
    return 0;
}
