#include <stdio.h>
#include <time.h>

int findMin(int arr[], int n){
	int i, m = arr[0];
	for(i = 1; i < n; i++){
		if(arr[i] < m) m = arr[i];
	}
	return m;
}

int findMax(int arr[], int n){
	int i, m = arr[0];
	for(i = 1; i < n; i++){
		if(arr[i] > m) m = arr[i];
	}
	return m;
}

float average(int arr[], int n){
	int i;
	float s = 0;
	for(i = 0; i < n; i++){
		s += arr[i];
	}
	return s/n;
}

int main(){
	const int N = 20;
	int mas[N];
	int i;
	
	srand(time(NULL));
	for(i = 0; i < N; i++){
    	mas[i] = rand() % 100;
    	printf("%d ", mas[i]);
	}
	
	printf("\nmax = %d\n", findMax(mas, N));
	printf("min = %d\n", findMin(mas, N));
	printf("average = %f\n", average(mas, N));
	return 0;
}
