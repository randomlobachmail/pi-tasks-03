#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(){
	const int N = 50;
	int arr[N];
	int i;
	
	srand(time(NULL));
	for(i = 0; i < N; i++){
    	arr[i] = 0 + rand() % 10;
    	printf("%d ", arr[i]);
	}

    int a = arr[0],
    	len = 1,
		max_len = 1;
		
	for(i = 1; i < N; i++){
		if(arr[i] == arr[i-1]){
			len++;
		} else {
			len = 1;
		}
		if(len >= max_len){
			max_len = len;
			a = arr[i];
		}
	}
	
	printf("\n\n%d\n", max_len);
	for(i = 0; i < max_len; i++)
    	printf("%d ", a);
	
    
	return 0;
}
