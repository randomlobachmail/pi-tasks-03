/*������. ������� � ������� ���������� �� ������� "return min,max,av;".
� � ����� ���� ����� ����� "printf("\nResults: Min=%d, Max=%d, Avarage=%f\n",min_max_av(arr));"
�� ������-�� � ������ ���������� ���� �������� min. Max � av (avarage) �� �����������. ��� � ����� �� ���?*/
#define N 20
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int min_max_av (int *arr)
{
	int min=INT_MAX,max=INT_MIN,i;
	float av=0;  //av-avarage
	for (i=0; i<N; i++)
	{
		if (arr[i]>max)
			max=arr[i];
		if (arr[i]<min)
			min=arr[i];
		av+=arr[i];
	}
	return printf("\nResults: Min=%d, Max=%d, Avarage=%.3f\n",min,max,av/N);
}
int main()
{
	int arr[N],i;
	srand(time(0));
	for (i=0; i<N; i++)	//array generation
	{
		arr[i]=rand()%100;
		printf("%d ",arr[i]);
	}
	min_max_av(arr);
	return 0;
}





