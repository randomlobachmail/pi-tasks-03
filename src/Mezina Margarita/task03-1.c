#include <stdio.h>
#include <time.h>
int main()
{
	int n=8, l = 0;
	double start, finish, t[8];
	start = clock();
	long i, m = 0, f = 0;
	for (i = 0; i < n; ++i) {
		m = m * 10 + 9;
		f = f + 9;
	}
	long a[73], h, k;
	for (i = 0; i <= f; ++i)
		a[i] = 0;
	for (i = 0; i <= m; ++i) {
		h = 0; k = i;
		while (k != 0) {
			h = h + k % 10;
			k = (int)k / 10;
		}
		++a[h];
		if (i == 9 || i == 99 || i == 999 || i == 9999 || i == 99999 || i == 999999 || i == 9999999 || i == 99999999) {
			finish = clock();
			t[l] = (finish - start) / 1000;
			++l;
		}
	}
	for (i = 0; i <= f; ++i)
		printf("%i - %i\n", i, a[i]);
	for (i=0; i<n; ++i) 
		printf("n=%i    %.3fsec\n", i+1, t[i]);
	return 0;
}