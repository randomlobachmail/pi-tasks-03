#define N 10
#define MINA -100
#define MAXA 100
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
void minimum(int a, long *min)
{
	if (*min > a)
		*min = a;
}
void maximum(int a, long *max)
{
	if (*max < a)
		*max = a;
}
int average(int a, double sr)
{
	return a + sr;
}
int main()
{
	srand(time(0));
	int a[N], i;
	long min = MAXA + 1, max = MINA - 1;
	double sr = 0;
	for (i = 0; i < N; ++i)
	{
		a[i] = rand() % (MAXA - MINA) + MINA;
		minimum(a[i], &min);
		maximum(a[i], &max);
		sr = average(a[i], sr);
	}
	sr = sr / N;
	printf("max=%i\n", max);
	printf("min=%i\n", min);
	printf("average=%.3f\n", sr);
	return 0;
}
