#define _CRT_SECURE_NO_WARNINGS
#define N 30
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void maxRandNumSeq(int *array, int size) // max number sequence - ������������ ������������������ ��������� �����
{
	int i, lengh = 1, maxLengh = 1, maxNum;
	srand(time(0));
	printf("Random number sequence: ");
	for (i = 0; i < size; i++)
	{
		array[i] = rand() % 10 + '0'; //��������� � ������������������ ��������� �����
		putchar(array[i]);
		putchar(' ');
	}
	for (i = 1; i <= size; i++)
	{
		if (array[i] == array[i - 1])
			lengh++;
		else
		{
			if (maxLengh < lengh)
			{
				maxLengh = lengh;
				maxNum = array[i - 1];
			}
			lengh = 1;
		}
	}
	if (maxLengh == 1)
	{
		printf("\nMax random number sequence: None\n");
		printf("Lengh of equal number's sequence is 0\n");
	}
	else
	{
		printf("\nLengh of equal number's sequence is %d\n", maxLengh);
		printf("Max random number sequence: ");
		for (i = 0; i < maxLengh - 1; i++)
			printf("%d, ", maxNum - 48); // ����������, ����� �������� ������ �����, ���� �� ����� ����� ������� ������ ��� ��������
		printf("%d\n", maxNum - 48);
	}
}

int main()
{
	int array[N];
	maxRandNumSeq(array, N);
	return 0;
}
