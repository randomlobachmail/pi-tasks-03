#define N 15
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

void maxMinAvg (int *array, int size) // ������� ���������� max, min � �������� �������� � ������� ��������� ����� 
{
	int i, avg,  max = 0, min, sum = 0;
	srand(time(0));
	printf("Random number sequence: ");
	for (i = 0; i < size; i++)
	{
		array[i] = rand() % 100; //��������� � ������������������ ��������� �����
		sum += array[i];
		printf("%d", array[i]);
		putchar(' ');
	}
	min = array[0];
	for (i = 1; i < size; ++i) // ���������� ��������� � �������� �����������������
	{
		if (array[i] < min)
			min = array[i];
		else if (array[i] > max)
			max = array[i];
	}
	avg = sum / N;
	printf("\nMax value is %d \nMin value is %d \nAverage value is %d\n", max, min, avg);
}

int main()
{
	int array[N];
	maxMinAvg(array, N);
	return 0;
}