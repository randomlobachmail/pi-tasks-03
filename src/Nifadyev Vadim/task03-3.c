#define N 256
#include <stdio.h>

void lettersInString(char *string, int size)
{
	long i;
	char letter;
	long number[N] = { 0 }; // ��������� ������ ������
	printf("Enter the string: ");
	fgets(string, N, stdin);
	for (i = 0; i < size; i++)
		if (string[i] >= '0' && string[i] <= '9')
		{
			printf("Input error!\n");
			return 1;
		}
	for (i = 0; i < strlen(string) - 1; i++)
	{
		number[string[i]]++;
		if (string[i] == ' ' || string[i] == '\t')
			number[string[i]] = 0;
	}
	for (i = 0; i < 256; ++i)
		if (number[i] != 0) 
		{
			letter = i;
			printf("%c-%i, ", letter, number[i]);
		}
	printf("\b\b \n"); // ������� ������� � �����
}

int main()
{
	char string[N];
	lettersInString(string, N);
	return 0;
}