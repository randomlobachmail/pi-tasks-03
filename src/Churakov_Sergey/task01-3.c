
#include "stdafx.h"
#include <math.h>
#include <time.h>
#define N 73

int main()
{
	clock_t start, finish;
	start=clock();
	printf("Enter a number");
	int suml, i, razryad;
	razryad = 0;
	scanf("%d", &razryad);	
	suml = 0;
	int arr[N];
	if (razryad > 8 || razryad < 1)
		return 1;
	else
	{
		for (i = 0;i <= 9 * razryad;i++)
			arr[i] = 0;
		for (i = 0;i < razryad;i++)
			suml = 10 * suml + 9;
		int sum;
		for (i = 0;i <= suml;i++)
		{
			sum = 0;
			while (sum)
			{
				sum +=suml % 10;
				suml /= 10;
			}
			arr[sum]++;
		}
	}
	for (i=0;i<=9*razryad;i++)
		printf("%d had been found %d times",i,arr[i]);
	finish=clock();
	double f;
	f=(double)(finish-start)/CLOCKS_PER_SEC;
	printf("\r %f", f);
    return 0;
}

