#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 15

// ���������� ������� ���������� �������.
void randArr(int *arr)
{
	for (int i = 0; i < N; i++)
		arr[i] = rand() % 5;
}

// ����� �������.
void outArr(int *arr)
{
	for (int i = 0; i < N; i++)
		printf("%d ", arr[i]);
	printf("\n");
}

// �������, ������� ��������� ����� ������������ ������������������.
void findMaxSq(int *arr, int *sqMax, int *sqValue)
{
	int currentSq = 0;
	int lastValue = -1;

	lastValue = arr[0];
	for (int i = 1; i < N; i++)
		if (arr[i] == lastValue)
			currentSq++;
		else
		{
			if (currentSq >= *sqMax)
			{
				*sqMax = currentSq;
				*sqValue = lastValue;
			}
			currentSq = 1;
			lastValue = arr[i];
		}
}

// ����� ������������ ������������������. 
void outSq(int max, int value)
{
	printf("Max sequence = %d \n", max);
	for (int i = 0; i < max; i++)
		printf("%d ", value);
	printf("\n");
}

int main()
{
	int arr[N] = { 0 };
	int sqMax = 0; // ������������ ������������������.
	int sqValue = 0; // �������� ������������������.

	srand(time(0));

	randArr(arr);
	outArr(arr);

	findMaxSq(arr, &sqMax, &sqValue);

	outSq(sqMax, sqValue);

	return 0;
}