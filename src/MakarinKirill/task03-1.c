#include <stdio.h>
#include <time.h>
#define N 73
#define T 9

// ������� ��������� ����� ��������� �� ������� ���������� 9.
int rankToNumber(int value)
{
	int result = 0;
	for (int i = 0; i < value; i++)
		result = result * 10 + 9;
	return result;
}

// �������, ������� ��������� ����� ���� � �����.
int sumOfDigit(int value)
{
	int sum = 0;
	while (value > 0)
	{
		sum += (value % 10);
		value /= 10;
	}
	return sum;
}

// ����� ������� ������ �����.
void outArr(int *qtSum, int max)
{
	for (int j = 1; j <=max ; j++)
		printf("%d \t ", qtSum[j]);
	printf(" \n");

}

// "���������" �������.
void setToZeroArr(int *qtSum)
{
	for (int k = 0; k < N; k++)
		qtSum[k] = 0;
}


int main()
{
	int qtSum[N]; // ������ ���-�� ���.
	int currentRank; // ���������� ��������.
	int i;
	int sum = 0; // ����� "�������".
	int number = 0; // ����� ��������� �� &currentRank "�������".
	clock_t start, finish;
	double time[T] = { 0. };

	// �������� ���� ���������, ���������� ������� [1..8]
	for (currentRank = 1; currentRank <= 8; currentRank++)
	{
		setToZeroArr(qtSum);
		sum = 0;
		number = 0;

		sum = currentRank * 9;
		number = rankToNumber(currentRank);

		start = clock();
		for (i = 0; i<=number; i++)
			if (sumOfDigit(i) <= sum)
				qtSum[sumOfDigit(i)]++;
		finish = clock();

		time[currentRank] = (double)(finish - start) / CLOCKS_PER_SEC;

		outArr(qtSum, sum);

		printf("Time for %d = %f \n", currentRank , time[currentRank]);
	}
	
	return 0;
}