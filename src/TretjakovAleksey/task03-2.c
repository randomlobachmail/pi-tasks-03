#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main()
{
	int i, j, length = 1, last = 1, kol = 0;
	int arr[256];
	srand(time(NULL));
	printf("Enter kol of numbers in the array 1 to 256: ");
	if (scanf("%d", &kol) == 0 || !(kol >= 1 && kol <= 256))
	{
		printf("Input error!\n");
		return 1;
	}
	for (i = 0; i < kol; i++)
	{
		arr[i] = rand() % (10 - 0 + 1) + 0;
		if (arr[i] == arr[i - 1])
			length++;
		else if (last == length)
			length = 1;
		else if (last < length)
		{
			last = length;
			j = arr[i - 1];
			length = 1;
		}
	}
	if (last < length)
	{
		last = length;
		j = arr[i - 1];
	}
	else if (last == 1)
		j = arr[0];
	printf("Array ready!\n");
	for (i = 0; i < kol; i++)
		printf("%d ", arr[i]);
	printf("\nResult ready!\n");
	for (i = 0; i < last; i++)
		printf("%d ", j);
	printf("\nLength: %d\n", last);
	return 0;
}