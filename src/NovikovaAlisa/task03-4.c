#define MAX_RANDOM_NUMBER 1000
#define N 15
#include <stdio.h>
#include <time.h>
int maximum(int *arr, int size){
	int t = 0;
	for (int i = 0; i < size; i++){
		if (arr[i]>t)
			t = arr[i];
	}
	return t;
}
int minimum(int *arr, int size){
	int m = MAX_RANDOM_NUMBER + 1;
	for (int i = 0; i < size; i++){
		if (arr[i]< m)
			m = arr[i];
	}
	return m;
}
float averagen(int *arr, int size){
	double sum = 0.0;
	int i = 0;
	for (i; i < size; i++){
		sum = sum + arr[i];
	}
	return (float)sum / size;
}
int main(){
	int arr[N];
	int i = 0;
	
	srand(time(NULL));
	for (i; i < N; i++){
		arr[i] = rand() % MAX_RANDOM_NUMBER;
		printf("%d ", arr[i]);
	}
	printf("\nMaximum = %d\n", maximum(&arr, N));
	printf("Minimum = %d\n", minimum(&arr, N));
	printf("Average = %.2f\n", averagen(&arr, N));
	return 0;
}
