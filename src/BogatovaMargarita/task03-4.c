#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int MinMaxMean(int *array, int lengthMMM, int *maxMMM, int *minMMM, int *meanMMM)
{
	int j = 0, sum = 0;
	for (j = 0; j < lengthMMM; j++)
	{
		if (array[j] > *maxMMM)
			*maxMMM = array[j];
		else if (array[j] < *minMMM) *minMMM = array[j];
		sum += array[j];
	}
	*meanMMM = sum / lengthMMM;
}
int main()
{
	int length = 0, i = 0, maxMMM = -1, minMMM = 102, meanMMM = 0;
	int array[1000];
	srand(time(0));
	scanf("%i", &length);
	for (i = 0; i < length; i++)
	{
		array[i] = rand() % (101);
		printf("%i ", array[i]);
	}
	MinMaxMean(array, length, &maxMMM, &minMMM, &meanMMM);
	printf("\n Max: %d  Min: %d  Arithmetical mean: %d \n", maxMMM, minMMM, meanMMM);
	return(0);
}