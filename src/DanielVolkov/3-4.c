#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include<time.h>
#include<stdlib.h>
void MAXMIN(int*arr, int size)
{
	int i;
	for (i = 0; i < size-1; i++)
	{
		if (arr[i]>arr[i + 1])
		{
			int temp = arr[i];
			arr[i] = arr[i + 1];
			arr[i + 1] = temp;
		}
	}
	printf("\nMAX=%d", arr[size-1]);
	for (i = size-2; i > 0; i--)
	{
		if (arr[i]<arr[i - 1])
		{
			int temp = arr[i];
			arr[i] = arr[i - 1];
			arr[i - 1] = temp;
		}
	}
	printf("\nMIN=%d\nSECONDARY=%d", arr[0],(arr[size-1]+arr[0])/2);
	
}
int main()
{
	int arr[20], i = 0;
	srand(time(NULL));
	while (i < 20)
	{
		arr[i] = rand() % 200;
		printf("%d ", arr[i]);
		i++;
	}
	MAXMIN(arr, 20);
	return 0;
}