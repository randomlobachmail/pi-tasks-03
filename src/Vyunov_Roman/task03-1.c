#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#define N 9
#define size 100

int SumOfNumber(int number)
{
	int sum = 0;
	while (number)
	{
		sum += number % 10;
		number /= 10;
	}
	return sum;
}
int AssemblyLoopNumber(int number, int loop)
{
	int res = number;
	for (int i = 1; i < loop; i++)
	{
		res *= 10;
		res += number;
	}
	return res;
}

int main(void)
{
	int arr[size];
	long int num, loop, raw_interval;
	printf("Enter number 1 to 8 :");//количество разрядов 9-99 999 999
	scanf("%d", &num);
	raw_interval = AssemblyLoopNumber(N, num);
	loop = SumOfNumber(raw_interval);
	for (int i = 0; i <= loop; i++)
	{
		arr[i] = 0;
		for (int j = 0; j <= raw_interval; j++)
		{
			if (i == SumOfNumber(j))
				arr[i]++;
		}
	}
	for (int i = 0; i <= loop; i++)
	{
		printf("%d\t%d\n", i,arr[i]);
	}
	return 0;
}