#define N 10
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
int maxVal(int *arr)
{
	int i, max=0;
	for(i=0;i!=N;i++)
		if(arr[i]>max)
			max=arr[i];
	return max;	
}
int minVal(int *arr)
{
	int i, min=99;
	for(i=0;i!=N-1;i++)
		if(arr[i]<min)
			min=arr[i];
	return min;	
}
float averVal(int *arr)
{
	int i;
	float sum=0;
	for(i=0;i!=N-1;i++)
		sum+=arr[i];
	return sum/N;	
}
int main()
{
	int i, sum=0, max, min; 
	float aver;
	int arr[N];
	srand (time (0));
	for (i=0;i<N-1;i++)
	{
		arr[i]=rand()%(99-0+1)+0;
		printf("%d ",arr[i]);
	}	
	max=maxVal(arr);
	min=minVal(arr);
	aver=averVal(arr);
	printf("\nMaximal value - %d \nMinimal value -  %d \nAverage value - %.1f",max, min, aver);
	return 0;
}

