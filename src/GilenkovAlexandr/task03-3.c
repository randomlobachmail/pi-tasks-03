#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>	
#define N 256

int main()
{
	char str[N];
	char ch;
	int i,j;
	int k;
	puts("enter the string:");
	fgets(str, N, stdin);
	for (i = 0; str[i+1] !='\0';)
	{
		if (str[i] == ' ')
			i++;
		else
		{
			k = 1;
			ch = str[i];
			str[i] = ' ';
			for (j = i + 1; str[j+1] != '\0'; j++)
			{
				if (str[j] == ch)
				{
					k = k + 1;
					str[j] = ' ';
					i++;
				}
			}
			printf("%c - %d\n", ch, k);
		}
	}
	return 0;
}