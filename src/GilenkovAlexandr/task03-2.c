#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#define N 100

int main()
{
	int mas[N];
	int count[11] = {0};
	int i,k=0;
	int len, lmax, val;
	srand(time(0));
	for (i = 0; i < N; i++)
		mas[i] = rand() % 10 + 1;
	for (i = 0; i < N; i++)
	{
		if (k < 10)
		{
			printf("%d,  ", mas[i]);
			k++;
		}
		else
		{
			printf("\n");
			printf("\n");
			printf("%d,  ", mas[i]);
			k = 1;
		}
	}
	putchar('\n');
	i = 1;
	len = 1;
    lmax = 1;
    val = mas[0];
	for (i = 0; i < N; i++)
	{
		if (mas[i] == val)
		{
			len++;
		}
		else
		{
			if (lmax <= len)
				lmax = len;
			val = mas[i];
			len = 1;
		}
	}
	printf("max len = %d\n val = %d", lmax, val);
	return 0;
}