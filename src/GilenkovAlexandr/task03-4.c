#include <stdio.h>
#include <time.h>
#define K 100 //������������ ������� ��� �������������
#define N 100 //������ �������


int maximum(int *mas, int len)
{
	int j, val=-1;
	for (j = 0; j < len; j++)
	{
		if (val < mas[j])
			val = mas[j];
	}
	return val;
}

int minimum(int *mas, int len)
{
	int j, val = K+1;
	for (j = 0; j < len; j++)
	{
		if (val > mas[j])
			val = mas[j];
	}
	return val;
}

int average(int *mas, int len, int maxi, int mini)
{
	int j, flag;
	int key=1;
	int val = (int)(maxi+mini)/2;
repeat:
	flag = 0;
	for (j = 0; j < len; j++)
	{
		if (val == mas[j])
		{
			flag = 1;
			break;
		}
	}
	if (flag == 0)
	{
		val+=key;    //��� �������� ����������, ����� ��������� �������
		key = -2;    //�����. �� min � max +-1
		goto repeat;
	}
	return val;
}

int main()
{
	int mas[N] = { 0 };
	int i;
	int max, min, mean; //������������, ����������� � ������� ��������
	srand(time(0));
	for (i = 0; i < N; i++)
	{
		mas[i] = rand() % K + 1;
	}
	max = maximum(mas, N);
	min = minimum(mas, N);
	mean = average(mas, N, max, min);
	printf("max = %d\n", max);
	printf("min = %d\n",  min);
	printf("mean = %d\n", mean);
	return 0;
}