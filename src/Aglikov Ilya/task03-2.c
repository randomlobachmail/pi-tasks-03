#define N 50
#include<stdio.h>
#include<time.h>

int main()
{
    int arr[N];
    int i, x, k, c=0, max=0;
    srand(time(0));
    for (i=0;i<N;i++)
        arr[i]=rand()%10;
    for(i=1;i<N;i++)
    {
        if(arr[i]==arr[i-1])
            c++;
        else c=1;
        if (c>max)
        {
            max=c;
            k=arr[i];
        }
    }
    printf("Max length = %d\n", max);
    for(i=0;i<max-1;i++)
        printf("%d, ", k);
    printf("%d", k);
    return 0;
}
