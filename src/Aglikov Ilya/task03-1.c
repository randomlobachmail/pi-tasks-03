#define N 73
#define M 8
#include <stdio.h>
#include <time.h>
int main()
{
	int j, i, s=0, num=0, val;
	int arr[N]={0};
	clock_t start, finish;
	double t;
	for(j=1;j<=M;j++)
	{
		for(i=0;i<j;i++)
			num=num*10+9;
		start=clock();
		for (i=0;i<=num;i++)
		{
			val=i;
			while(val)
			{
				s+=val%10;
				val/=10;
			}
			arr[s]++;
			s=0;
		}
		finish=clock();
		t=(double)(finish-start) / CLOCKS_PER_SEC;
		printf("%d - %lf\n", j, t);
		num=0;
		val=0;
	}
	return 0;
}
