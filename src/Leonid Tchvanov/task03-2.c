#define MAX_RANDOM_NUMBER 10
#define N 100
#include <stdio.h>
#include <time.h>

int main()
{
    unsigned int arr[N];
    int i, currentSequenceLength=0, maxSequenceLength=0, maxSequenceNumber;
    srand(time(0));
    for (i=0; i<=N; i++)
    {
        arr[i] = rand()%MAX_RANDOM_NUMBER;
    }
    for (i=1; i<=N; i++)
    {
        if (arr[i]==arr[i-1])
            if (currentSequenceLength==0)
                currentSequenceLength = 2;
            else
                currentSequenceLength++;
        else
        {
            if (maxSequenceLength<currentSequenceLength)
            {
                maxSequenceLength = currentSequenceLength;
                maxSequenceNumber = arr[i-1];
            }
            currentSequenceLength = 0;
        }
    }
    if (maxSequenceLength>0)
        {
            printf("Maximum sequence length is %d\n", maxSequenceLength);
            for (i=0; i<maxSequenceLength-1; i++)
                {
                    printf("%d, ", maxSequenceNumber);
                }
            printf("%d", maxSequenceNumber);
        }
    return 0;
}
